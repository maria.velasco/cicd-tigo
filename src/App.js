
import React, { Component } from 'react';
import './App.css';
class App extends Component {

  state = {
    data : []
  }

  componentDidMount = async () => {
    // Actualiza el título del documento usando la API del navegador
    const requestOptions = {
        method: 'GET',
        headers: { 'Content-Type': 'application/json' },
    }
    const res = await fetch("https://jsonplaceholder.typicode.com/posts",requestOptions);
    const posts = await res.json();
    this.setState({data:posts});
    console.log(this.state.data);
  };



  render(){
    return (
      <div className="container">
        {this.state.data.map((post)=>{
          return (
            <div key={post.id} className="post">
              <h1>{post.title}</h1>
              <p>{post.body}</p>
            </div>
          )
        })}
      </div>
      );
    }
}

export default App;
