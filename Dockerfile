FROM node:14-alpine

WORKDIR /usr/src/app

RUN set -xe \
  ;apk add --no-cache \
    yarn 

RUN npm install -g @nestjs/cli

COPY . .

RUN yarn install --immutable

RUN yarn build

CMD ["yarn", "start"]